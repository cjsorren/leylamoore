<!DOCTYPE html>
<html>
    <head>
        <meta charset='UTF-8'>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/shorticon.ico">
        <title>
            <?php
                wp_title( '-', true, 'right');
                
            ?>
        </title>
        <meta name="viewport" content="initial-scale=1">
        <?php wp_head(); ?>
    </head>
    <body class="home">
        <div class="page-wrap">

            <div class="header">

                <div class="nav_wrap">
                <div class="inner nav">

                    <div class="lmd_logo">
                        <a href="<?php echo get_option('home'); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/lmd_logo.svg" onerror="this.src='lmd_logo.png'; this.onerror=null;">
                    </div>
                    
                    <a href="#menu" class="menu-link justify-right">MENU <i class="fa fa-bars" ></i></a>
                    <nav id="menu" role="navigation">
                        <?php wp_nav_menu( array( 
                            'theme_location' => 'primary_nav' , 
                            'container_class' => '' ,
                            'menu_class' => 'main-nav-right justify-right',
                            'menu_id' => '', ) ); ?>
                    </nav>
                    



                </div>
            </div>

            </div>

            

