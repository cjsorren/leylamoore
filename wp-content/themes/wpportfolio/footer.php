        <footer class="site-footer">
            <div class="inner footer">

            <div class="social">
                <a href="<?php the_field( 'facebook', 16); ?>" class="icon-alone" target="_blank">
                    <span aria-hidden="true" data-icon="f"></span>
                    <span class="screen-reader-text">facebook</span>
                </a>
                <a href="<?php the_field( 'twitter', 16); ?>" class="icon-alone" target="_blank">
                    <span aria-hidden="true" data-icon="t"></span>
                    <span class="screen-reader-text">twitter</span>
                </a>
                <a href="<?php the_field( 'linkedin', 16); ?>" class="icon-alone" target="_blank">
                    <span aria-hidden="true" data-icon="l"></span>
                    <span class="screen-reader-text">linkedin</span>
                </a>
            </div>

            <p class="email">
            <a href="mailto:<?php the_field( 'email', 16); ?>"><?php the_field( 'email', 16); ?></a>
            </p>
            <p>&copy; <?php echo date('Y '); ?> Leyla Moore
            </p>
    
    
            </div>
        </footer>

        <?php wp_footer(); ?>
    </div>

</body>
        
</html>