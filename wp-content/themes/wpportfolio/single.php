<?php get_header(); ?>

<div class="tagline-wrap">
    <div class="tagline">
        <h1>graphic design for good causes</h1>
    </div>
</div>

<!--Blog-->

<div class="blog-wrapper">
  
      <!-- Primary Column -->
      <div class="blog">
        <div class="blog-post">
    
		    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <article <?php post_class('post'); ?>>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <ul class="post-meta no-bullet">
                  <li class="cat">in <?php the_category( ', ' ); ?></li>
                  <li class="date">on <?php the_time('F j, Y'); ?></li>
                </ul>    
                <?php if( get_the_post_thumbnail() ) : ?>
                <div class="img-container">
                  <?php the_post_thumbnail('large'); ?>
                </div>  
                <?php endif; ?>

                <?php the_content(); ?>

              </article>

			<?php endwhile; else : ?>
			
			  <p><?php _e( 'Sorry, no pages found.' ); ?></p>
			
			<?php endif; ?>
    
		    </div>
	  
      </div>

</div>



<?php get_footer(); ?>