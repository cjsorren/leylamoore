<?php

add_theme_support( 'post-thumbnails' );

//Load Theme CSS
function theme_styles () {
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css');
    wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'montserrat', 'http://fonts.googleapis.com/css?family=Montserrat:400,700');
    wp_enqueue_style( 'asap', 'http://fonts.googleapis.com/css?family=Asap:400,700,400italic,700italic');
    wp_enqueue_style( 'fontawesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    
}

//Load Theme JS
function theme_js () {
    wp_register_script( 'navmenu_js', get_template_directory_uri() . '/js/menu.js', array( 'jquery' ), '', true);
    wp_enqueue_script( 'navmenu_js' );

    wp_register_script( 'isotope_js', get_template_directory_uri() . '/js/isotope.js', array( 'jquery' ), '', true);
    wp_enqueue_script( 'isotope_js' );

    wp_register_script( 'imagesloaded_js', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array( 'jquery' ), '', true);
    wp_enqueue_script( 'imagesloaded_js' );



    wp_register_script( 'filter_js', get_template_directory_uri() . '/js/filter_test.js', array( 'jquery' ), '', true);
    wp_enqueue_script( 'filter_js' );



}

add_action( 'wp_enqueue_scripts', 'theme_js');



add_action( 'wp_enqueue_scripts', 'theme_styles');

// Enable custom menus
add_theme_support( 'menus');

//Admin Color Scheme
function wpt_admin_color_schemes() {

    $theme_dir = get_stylesheet_directory_uri();

    wp_admin_css_color(
        'leyla', __( 'Leyla'),
        $theme_dir . '/admin-colors/leyla/colors.css',
        array( '#384047', '#5BC67B', '#838cc7', '#ffffff')
    );

}
add_action ('admin_init', 'wpt_admin_color_schemes');

//strip wordpress image width and height
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

// Add Read More Link
function new_excerpt_more($more) {
       global $post;
    return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read more...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Register Menu

function register_primary_nav_menu() {
    register_nav_menu('primary_nav',__( 'Primary Nav' ));
}
add_action( 'init', 'register_primary_nav_menu' );

// Remove Home from menu
function leylamooredesign_nav_menu_args( $args ) {
    //$args['show_home'] = false;
    $args['container'] = false;
    return $args;
}
add_filter( 'wp_nav_menu_args', 'leylamooredesign_nav_menu_args' );

?>



