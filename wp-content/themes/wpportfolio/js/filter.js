jQuery(document).ready(function(){

  // init Isotope

  var $container = jQuery('.gallery').isotope({

  // main isotope options
  itemSelector: '.box',
  masonry: {
    gutter: 0,
    columnWidth: '.box',
    percentposition:true}

  // options
  });



  // filter items on button click
  jQuery('#filters').on( 'click', 'button', function() {
    var filterValue = jQuery(this).attr('data-filter');
    $container.isotope({ filter: filterValue });
  });

  // set htm: overflow-y if necessary
  if ( document.querySelector('body').offsetHeight > window.innerHeight ) {
    document.documentElement.style.overflowY = 'scroll';
  }

//the media query will be processed before isotope is initialized, and then it read the value of the column width and initialize isotope with that property.

// var colWidth = jQuery('.box').width();

// jQuery('.gallery').isotope(
//  masonry: {
//    columnWidth: colWidth}
// });





});