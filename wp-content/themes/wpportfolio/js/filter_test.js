jQuery(document).ready(function(){

  // init Isotope

  var $container = jQuery('.gallery').isotope({

    // main isotope options
    itemSelector: '.box',
    masonry: {
    gutter: 0,
    columnWidth: '.box',
    percentposition:true}

    // options
  });


  // layout Isotope again after all images have loaded
  $container.imagesLoaded( function() {
    $container.isotope('layout');
  });



  // filter items on button click
  jQuery('#filters').on( 'click', 'button', function() {
    var filterValue = jQuery(this).attr('data-filter');
    $container.isotope({ filter: filterValue });
  });

  // change is-checked class on buttons
    jQuery('.button-group').each( function( i, buttonGroup ) {
      var $buttonGroup = jQuery( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        jQuery( this ).addClass('is-checked');
      });
    });

});





//hashfilter

function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent( hashFilter );
}

jQuery( function() {

  var $container = jQuery('.gallery');

  // bind filter button click
  var $filters = jQuery('#filters').on( 'click', 'button', function() {
    var filterAttr = $( this ).attr('data-filter');
    // set filter in hash
    location.hash = 'filter=' + encodeURIComponent( filterAttr );
  });

  var isIsotopeInit = false;

  function onHashchange() {
    var hashFilter = getHashFilter();
    if ( !hashFilter && isIsotopeInit ) {
      return;
    }
    isIsotopeInit = true;
    // filter isotope
    $container.isotope({
      itemSelector: '.box',
      filter: hashFilter
    });
    
  }

  jQuery(window).on( 'hashchange', onHashchange );
  // trigger event handler to init Isotope
  onHashchange();
});