<?php get_header(); ?>

<div class="tagline-wrap">
    <div class="tagline">
        <h1>graphic design for good causes</h1>
    </div>
</div>

<section class="project">

<div class="project-wrap">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="sidebar-right">
        <div class="project-header">
            <h2 class="client-name"><?php the_field( 'caption'); ?></h2>

        </div>
    </div>
        <div class="project-img-header">
            <img class="project-image" src="<?php the_field( 'project_featured_image'); ?>"/>
        </div>
    
    <div class="sidebar-right">
        <div class="project-description"><?php the_field( 'description'); ?></div>

        <span class="filter testimonial-filter">
            <?php
                $post_cats = get_the_category();
                if ($post_cats) {
                    $catlist = "";
                    foreach($post_cats as $category) {
                        $catlist .=  '<a href="/work/#filter=.' . $category->slug . '" data-filter=".' . $category->slug . '">' . $category->slug . '</a>, ';
                    }
                    echo rtrim($catlist, ", ");
                } ?>
        </span>
        

        <?php if( get_field( "testimonial" ) ): // only show testimonial if it exists ?>

        <div class="testimonial">
            
            
            <div class="quotes">“</div>
            <div class="quote-right"><p class="testimonial-quote"><?php the_field( 'testimonial'); ?>”</p>
            <p class="testimonial-name">&ndash; <?php the_field( 'name_testimonial'); ?></p>
            <p class="testimonial-position"><?php the_field( 'position_testimonial'); ?></p>
            <p class="testimonial-organization"><?php the_field( 'organization_testimonial'); ?></p></div>
            
        </div>
        <?php endif; ?>
    </div>

    <div class="project-images">

        <?php

            $images = get_field('project_gallery');

            if( $images ): ?>
                    <ul>
                        <?php foreach( $images as $image ): ?>
                                <img class="project-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        <?php endforeach; ?>
                    </ul>

        <?php endif; ?>

    </div>




<?php endwhile; else: ?>

<p>There are no posts or pages here</p>

<?php endif; ?>

</div>

</section>

<?php get_footer(); ?>