<?php

/*

    Template Name: Mission Page

*/


get_header(); ?>

<!--Mission Statement-->

        <div class="mission-statement">

            <p>I believe <span class="bold">design</span> has <br>the <span class="bold">power</span> to change minds and<br> <span class="bold">inspire action,</span> and I intend<br> to only use that power <span class="bold">for good.</span>

        </div>

<!--About Section-->
    <div class="mission-wrapper">
        <section class="mission-about">

            <div class="about-img">
                <img src="<?php the_field( 'about_image'); ?>">
            </div>

            <div class="about-wrapper">
                <div class="about-text">
                    <h1><?php the_field( 'about_headline'); ?></h1>

                    <p><?php $og_text= get_post_meta($post->ID, 'about_text', true);
                            echo wpautop( $og_text, $br );
                        ?>
                    </p>

                </div>
            </div>


        </section>
    </div>

<!--Footer-->

<?php get_footer(); ?>