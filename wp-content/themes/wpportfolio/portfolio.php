<div class="sub-nav wrap">

    <div id="filters" class="sub button-group">
        <span class="subnav align-left">
            <span class="filter">TYPE</span>
            <button class="filter button" data-filter=".print"><span class='border-bottom-underline'>PRINT</span></button>
            <button class="filter button" data-filter=".identity"><span class='border-bottom-underline'>IDENTITY</span></button>
            <button class="filter button" data-filter=".digital"><span class='border-bottom-underline'>DIGITAL</span></button>
        </span>
        <span class="subnav align-right">
            <span class="filter">FOR THE</span>
            <button class="filter button" data-filter=".people"><span class='border-bottom-underline'>PEOPLE</span></button>
            <button class="filter button" data-filter=".planet"><span class='border-bottom-underline'>PLANET</span></button>
            <button class="filter button" data-filter=".arts"><span class='border-bottom-underline'>ARTS</span></button>
        </span>
    </div>

</div>

<section class="gallery-wrap">

    <div class="gallery inner">





<?php

    $args = array (
        'post_type' => 'work'
    );

    $the_query = new WP_Query( $args );

?>



<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


            <div class="box <?php $post_cats = get_the_category(); foreach( $post_cats as $category ) { echo $category->slug.' ';} ?>" data-category="<?php $post_cats = get_the_category(); foreach( $post_cats as $category ) { echo $category->slug.' ';} ?>">
                <a href="<?php the_permalink() ;?>">
                    <img src="<?php the_field( 'gallery_thumb'); ?>"/>
                    <span class="caption fade-caption">
                        <hgroup>
                        <h3><?php the_field( 'caption' ); ?></h3>
                        <h2>
                            <?php
                                $post_cats = get_the_category();
                                if ($post_cats) {
                                    $catlist = "";
                                    foreach($post_cats as $category) {
                                        $catlist .=  $category->slug . ', ';
                                    }
                                    echo rtrim($catlist, ", ");
                                } ?>
                        </h2>
                        </hgroup>
                    </span>
                </a>
            </div>






<?php endwhile; else: ?>

    <p>There are no images to show</p>

<?php endif;?>

    </div>

</section>