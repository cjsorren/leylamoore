<?php

/*

    Template Name: Mission Page

*/


get_header(); ?>

<div class="tagline-wrap">
    <div class="tagline">
        <h1>graphic design for good causes</h1>
    </div>
</div>

<!--Contact-->
    <div class="contact-wrapper">
        <div class="contact">
            <div class="contact-text">
                <p><?php the_field( 'contact_text'); ?></p>
            </div>

            <ul class="contact-links">
                <li><a href="mailto:<?php the_field( 'email'); ?>"><span class="icon-contact" aria-hidden="true" data-icon="e"></span><?php the_field( 'email'); ?></a></li>
                <li><a href="<?php the_field( 'facebook'); ?>" target="_blank"><span class="icon-contact" aria-hidden="true" data-icon="f"></span>LeylaMooreDesign</a></li>
                <li><a href="<?php the_field( 'twitter'); ?>" target="_blank"><span class="icon-contact" aria-hidden="true" data-icon="t"></span>LeylaDesign</a></li>
                <li><a href="<?php the_field( 'linkedin'); ?>" target="_blank"><span class="icon-contact" aria-hidden="true" data-icon="l"></span>LeylaMoore</a></li>
            </ul>

        </div>
    </div>

<!--Footer-->

<?php get_footer(); ?>